\1. Go to local GitLab (gitlab.specs.com.ng/192.168.19.16:8510)

\2. Go to Source Group

3.(a) open up Subgroup/ Projects

 (b) For Subgroup - create all corresponding projects on destination GitLab (gitlab.com)

\4. On Destination Gitlab (after corresponding projects & subgroups have been created). Get the clone URL by clicking on the clone pop up and select "Clone with HTTPS"

5.on Source Gitlab click on settings, then click on "Repository"

\6. Navigate to "Mirroring Repositories" and then click on Expand

\7. Paste the URL cloned/copied from the corresponding GitLab destination (from step 4)

\8. Include the username in the URL "https://username@gitlab.company.com/group/project.git"

\9. Set Mirror direction to "Push" and Authentication Method to "Password". User is expected to enter their password

\10. Then click on "Update Now" or the recycle symbol ![pastedGraphic.png](blob:file:///228566d9-572e-4a2c-bb65-1b5054807e7a)



| **Project/Group**      | **URL**                                              |
| ---------------------- | ---------------------------------------------------- |
| Specs Apps             | https://gitlab.com/specs-apps                        |
| Human Manager          | https://gitlab.com/specs-apps/human-manager          |
| Rd-projects            | https://gitlab.com/specs-apps/rd-projects            |
| Remita-stp             | https://gitlab.com/specs-apps/remita-stp             |
| Automation Testing     | https://gitlab.com/specs-apps/automation_testing     |
| Devsite                | https://gitlab.com/specs-apps/devsite                |
| Payment Infrastructure | https://gitlab.com/specs-apps/payment-infrastructure |
| RemitaCore             | https://gitlab.com/specs-apps/remitacore             |
| RemitaERPV2            | https://gitlab.com/specs-apps/remitaerpv2            |
| RemitaMicroServices    | https://gitlab.com/specs-apps/remitamicroservices    |
| RemitaMobile           | https://gitlab.com/specs-apps/remitamobile           |
| RemitaMobileIOSApp     | https://gitlab.com/specs-apps/remitamobileiosapp     |
| RemitaPaymentLink      | https://gitlab.com/specs-apps/remitapaymentlink      |
| RemitaSoaParentV2      | https://gitlab.com/specs-apps/remitasoaparentv2      |
| website                | https://gitlab.com/specs-apps/website                |
| It-governance          | https://gitlab.com/specs-apps/it-governance          |

